global_fun = lambda p: print(f"invoke  global lambda ... {p}")


class Category:
    cat_par = "666"

    # 此处的p1 绑定的是 self 实例
    cate_fun = lambda p1: print(f"invoke category method {p1}")

    def method(self) -> None:
        print(self.cat_par)
        print(Category.cat_par)

    @staticmethod
    def s_method() -> None:
        print(Category.cat_par)


if __name__ == '__main__':
    global_fun("fkit")
    category = Category()
    category.cate_fun()
    print("*" * 10)
    category.method()
    category.s_method()
    print("*" * 10)
    # 此处会报错，强调了 self 调用的就必须要实例来调用
    # Category.method()
    Category.s_method()
    print("*" * 10)
    Category.cat_par = "777"
    print(Category.cat_par)
    print(category.cat_par)
    print("*" * 10)
    # 通过实例的来赋值，相当于 定义了新的实例变量
    category.cat_par = "888"
    print(Category.cat_par)
    print(category.cat_par)
