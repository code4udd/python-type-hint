"""
python 小技巧：
1、dic 的合并：利用 x.update(y) ，合并后结果在 x 中
"""


# 合并多个 dic
def merge_dic(*dic_args):
    r_dic = {}
    for x in dic_args:
        r_dic.update(x)
    return r_dic
