"""
1、python 中的实例的字段都是动态添加的，原本定义在类中的都是类的变量。
2、python 中如果动态添加的变量是通过 __ 来修饰，那么可以理解是私有的。
"""


class User:
    def __init__(self, name: str):
        self.set_name(name)

    def get_name(self) -> str:
        return self.__name

    def set_name(self, name: str):
        if len(name) < 2:
            raise ValueError("name length must rather than 2")
        self.__name = name


# python 查看错误栈，直接看最后一个，应该最清晰
if __name__ == '__main__':
    us = User("l")
    print(us.get_name())
