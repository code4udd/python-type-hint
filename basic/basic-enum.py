import enum


class Season(enum.Enum):
    pass


Season = enum.Enum(Season, ("Spring", "SUMMER"))

if __name__ == '__main__':
    print(Season.Spring)
