"""
python中的属性，property
"""


class Rectangle:

    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height

    def setsize(self, size: tuple):
        self.width, self.height = size

    def getsize(self):
        return self.width, self.height

    def delsize(self):
        self.width = 0
        self.height = 0

    # py中的 property 类似属性的聚合...
    size = property(getsize, setsize, delsize, "测试属性")

    size_another = property(getsize, None, None, "测试的其他属性")


if __name__ == '__main__':
    print(Rectangle.__doc__)
    help(Rectangle.size)

    rec = Rectangle(4, 5)

    print(rec.size)
    rec.size = (7, 9)
    print(rec.getsize())
    # 此处默认调用的是 rec.size
    del rec.size
    # rec.delsize()
    print(rec.getsize())
    print("*" * 10)
    rec = Rectangle(6, 8)
    print(rec.size_another)
    rec.size_another = 9, 10
    print(rec.size_another)
    print("xxxx")
