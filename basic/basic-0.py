def top_method() -> str:
    print("i am top method")


top_num: int = 10


class Person:
    # 类变量
    hair: str = "black"

    # 假如这么定义
    # 1.ide建议定义为top方法
    # 2.通过注解定义为 @staticmethod
    @staticmethod
    def sta_method(content: str):
        print(f"sta method..{content}")

    @classmethod
    def cls_method(cls):
        print("cls method...")

    # 此处的本质是动态给 python 添加 2 个对象属性
    def __init__(self, name: str, age: int) -> None:
        self.name = name
        self.age = age

    def __str__(self) -> str:
        return f"name:{self.name} age:{self.age}"

    def say(self, content) -> None:
        print(f"{self.name} say age is {self.age} and {content}")

    # typeHint 的写法是，self 不用管
    # 返回的如果是类，那么通过 双引号 括起来
    def grow_age(self) -> "Person":
        # python 中调用方法，如果是顶层函数，一定要注意第一个参数是不是 self
        if hasattr(self, "age"):
            self.age += 1
        else:
            self.age = 1
        return self


if __name__ == '__main__':
    p = Person("x", 10)
    print(p)
    p.say("hello china")
    p.grow_age().grow_age().grow_age()
    print(p.age)
    print("*"*10)
    Person.cls_method()
    p.cls_method()
    print("*"*10)
    Person.sta_method("hello")
    p.sta_method("hello")
