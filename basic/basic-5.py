"""
python 的 AOP 切面的类似实现，通过 函数修饰器进行实现
"""


def auth(fn):
    def auth_fn(*args):
        print(f"--- 模拟检查:{args[0]}")
        fn(*args)

    return auth_fn


@auth
def apply_pwd(tk: str) -> str:
    print(f"当前 token 是:{tk}")
    print("...do something...")
    return "22"


if __name__ == '__main__':
    apply_pwd("tk")
