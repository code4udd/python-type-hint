from typing import List, Iterable, Type, Tuple
import random

SUITS = "♠ ♡ ♢ ♣".split()
RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()
USERS = "A B C D".split()


class Card:
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank


class Deck:
    def __init__(self, cards: List[Card]):
        self.cards = cards


class CardHelper:

    @classmethod
    def deal_hands(cls, card_req_list: List[Card]) -> Tuple["Deck", "Deck", "Deck", "Deck"]:
        return Deck(card_req_list[0::4]), Deck(card_req_list[1::4]), Deck(card_req_list[2::4]), Deck(
            card_req_list[3::4])
        # d1 = Deck(card_req_list[0::4])
        # d2 = Deck(card_req_list[1::4])
        # d3 = Deck(card_req_list[2::4])
        # d4 = Deck(card_req_list[3::4])
        # return d1, d2, d3, d4


if __name__ == '__main__':
    card_list = [Card(s, r) for s in SUITS for r in RANKS]
    t4 = CardHelper.deal_hands(card_list)
    print(t4)
