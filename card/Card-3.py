# 定义一张卡牌
from typing import List, Iterable, Type, Tuple
import random

SUITS = "♠ ♡ ♢ ♣".split()
RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()
USERS = "A B C D".split()


class Card:
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return self.suit + self.rank


class Deck:
    def __init__(self, cards: List[Card]):
        self.cards = cards

    def __str__(self) -> str:
        d_card_list_str = [str(dc) for dc in self.cards]
        return " ".join(d_card_list_str)


class CardHelper:

    # 洗牌
    @classmethod
    def create_deck(cls) -> "Deck":
        card_list: List["Card"] = [Card(s, r) for s in SUITS for r in RANKS]
        # join 只能处理字符串，所以需要手动转为字符串处理。
        card_list_str = [str(c) for c in card_list]
        # print(" ".join(card_list_str))
        random.shuffle(card_list)
        card_list_str = [str(c) for c in card_list]
        # shuffle 不会返回任何值的
        # return Deck(random.shuffle(card_list))
        return Deck(card_list)

    # 发牌,先默认是 4 个人吧，后面再扩展
    @classmethod
    def deal_hands(cls, card_req_list: List[Card]) -> Tuple["Deck", "Deck", "Deck", "Deck"]:
        return Deck(card_req_list[0::4]), Deck(card_req_list[1::4]), Deck(card_req_list[2::4]), Deck(
            card_req_list[3::4])
        # d1 = Deck(card_req_list[0::4])
        # d2 = Deck(card_req_list[1::4])
        # d3 = Deck(card_req_list[2::4])
        # d4 = Deck(card_req_list[3::4])
        # return d1, d2, d3, d4


class User:

    # 脚本语言，从上倒下执行，所以会有顺序
    def __init__(self, u_name: str, req_deck: "deck"):
        self.u_name = u_name
        self.deck = req_deck

    def __str__(self):
        return self.u_name + str(self.deck)


# 定义
class Game:
    def __init__(self, user: str, req_deck: "Deck"):
        self.user = user
        self.deck = req_deck

    def __str__(self):
        return str(self.user) + ":" + str(self.deck)


if __name__ == '__main__':
    card_list = [Card("a", "1"), Card("b", "2")]
    # deck = Deck(card_list)
    deck = CardHelper.create_deck()
    four_cards = CardHelper.deal_hands(deck.cards)

    u_d_dict = dict(zip(USERS, four_cards))
    for u, c in u_d_dict.items():
        card_str_list = [str(c) for c in c.cards]
        card_str = " ".join(card_str_list)
        print(f"{u} cards:{card_str}")
