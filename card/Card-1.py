import random

# 定义常量
SUITS = "♠ ♡ ♢ ♣".split()
RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()


# python 中的 bool 是大写？
def create_deck(shuffle: bool = False):
    deck: tuple[str, str] = [(x, y) for x in SUITS for y in RANKS]
    if shuffle:
        random.shuffle(deck)
    return deck


# 发牌
def deal_hands(decktuple: [str, str]):
    return [decktuple[0::4], decktuple[1::4], decktuple[2::4], decktuple[3::4]]


# 玩牌
def paly():
    deck = deal_hands(create_deck(True))
    player = "abcd"
    cards = dict(zip(player, deck))
    # print(cards)
    for (name, cards) in cards.items():
        list_str = [f"{s}{r}" for (s, r) in cards]
        print(list_str)
        # 通过空格来分隔list
        result = " ".join(list_str)

        # print(f"{name} cards: {result}")
        # card_list=" ".join(f"{s}{r}" for (s,r) in cards)
        # print(f"{name} cards: {card_list}")


if __name__ == '__main__':
    paly()
