from datetime import date


class Animal:

    def __init__(self, name: str, birth: date) -> None:
        self.name = name
        self.birth = birth

    def twin(self, name: str) -> "Animal":
        return self.__class__(name, self.birth)

    @classmethod
    def new_born(cls, name: str) -> "Animal":
        # 此处类似 通过 class new 出一个实例
        return cls(name, date.today())


class Dog(Animal):
    def bark(self) -> None:
        print(f"{self.name} say woof!")


if __name__ == '__main__':
    mark = Dog.new_born("mark")
    flutter = mark.twin("flutter")
    mark.bark()
    flutter.bark()
