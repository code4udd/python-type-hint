from work.custom_util import first


class Person:
    def __init__(self, name, email, address):
        self.name = name
        self.email = email
        self.address = address

    def __str__(self):
        return f"name:{self.name} email:{self.email} address:{self.address}"

    def eat(self):
        print(f"{self.name} 在吃饭！")


class PersonRepository:
    list = {Person("lcf", "lcf@qq.com", "xxx1"),
            Person("wss", "wss@qq.com", "xxx2"),
            Person("hmm", "hmm@qq.com", "xxx3")}

    @staticmethod
    def find_by_name(name: str) -> "Person":
        for i in PersonRepository.list:
            # print(i)
            if i.name == name:
                return i
        # for 循环如果没有找到，才会执行该语句
        raise ValueError("未找到对应的额值")

    @staticmethod
    def find_by_email(email: str) -> "Person":
        # 此处性能不够好，如果有 findfirst 之类的就非常好了
        iter_res = filter(lambda item: item.email == email, PersonRepository.list)
        res_list = list(iter_res)

        if len(res_list) == 0:
            raise ValueError("未找到对应的额值")
        return res_list[0]

    @staticmethod
    def find_by_address(address: str) -> "Person":
        cur_p = first(lambda i: i.address == address, PersonRepository.list)
        if cur_p is None:
            raise ValueError("未找到对应的额值")
        return cur_p


if __name__ == '__main__':
    person = PersonRepository.find_by_name("hmm")
    print(person)
    p2 = PersonRepository.find_by_email("wss@qq.com")
    print(p2)
    p3 = PersonRepository.find_by_address("xxx3")
    print(p3)
    p3 = PersonRepository.find_by_address("xxx5")
    print(p3)
