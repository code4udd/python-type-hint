from typing import Any, Callable, Iterator


def first(item_check: Callable[[Any], bool], req_list: Iterator) -> Any:
    for i in req_list:
        if item_check(i):
            return i
    return None


if __name__ == '__main__':
    one_list = [1, 3, 4]
    result = first(lambda x: x == 1, one_list)
    print(result)
