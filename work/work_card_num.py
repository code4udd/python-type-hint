"""
输入身份证
1、判定是否合法
2、判定性别
3、判定出生年月
"""

# 加权因子
yz = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
# 校验因子
jy = ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"]


def check(card_num: str) -> None:
    # 1、判定长度
    if len(card_num) != 18:
        raise ValueError(f"身份证中应该为 18 位")

    # 2、判定每一位是否符合要求
    for i in card_num:
        # 此处不能是 1 和 10
        if "0" <= i <= "9":
            continue
        else:
            raise ValueError(str.format(f"身份证中，字符{i}不合法!"))

    # 3、计算校验因子
    sum_res = 0
    for i in range(len(card_num) - 1):
        sum_res = sum_res + yz[i] * int(card_num[i])
    res_want = sum_res % 11

    # 4、判定检验因子
    if jy[res_want] == str(card_num[17]):
        print("合法")
    else:
        raise ValueError("身份证中，不合法!")


if __name__ == '__main__':
    print("输入身份:")
    check(input())
