if __name__ == '__main__':
    # 遍历 list
    # 1-1、直接遍历到对应的元素，不依赖索引
    list_x = [1, 2, 3, 5]
    for item in list_x:
        print(item)

    print("*" * 10)
    # 1-2、好处是可以获取到索引，需要的时候需要用到
    for index in range(len(list_x)):
        print(f"{index} value is {list_x[index]}")

    print("enumerate")
    for (index, item) in enumerate(list_x, start=0):
        print(f"{index} value is {item}")
    print("enumerate")

    print("*" * 10)
    # 2-1、遍历 set,不能通过下标来访问 set
    set_x = {1, 3, 4}
    for item in set_x:
        print(f"value is {item}")

    print("*" * 10)
    # 3-1、遍历字典
    dic_x = {"a": 1, "b": 2, "c": 3}
    for (k, v) in dic_x.items():
        print(f"key and value={k}:{v}")

    print("*" * 10)
    for k in dic_x.keys():
        print(f"key:{k}")

    print("*" * 10)
    for v in dic_x.values():
        print(f"value:{v}")

    result = [x ** 2 for x in range(3)]
    result = [x + 2 for x in [1, 2, 3]]
    # 遍历的元组，元组也可以被遍历
    result = [x - 1 for x in (1, 3, 4)]

    # 玩点高难度的2 层循环

    result = [x + y for x in range(2) for y in range(3)]
    list_result = []
    for x in range(2):
        for y in range(3):
            list_result.append(x + y)
    print(list_result)
    # 再玩点高级的,类似 java 的 filter
    result = [x for x in range(10) if x % 2 == 0]
    result = [x for x in range(10) if x % 2 == 0 and x % 3 == 0]
    result = [x for x in range(10) if x % 2 == 0 if x % 3 == 0]
    # 仔细研究了一下才懂的
    result = [x ** 2 if x % 2 == 0 else x + 2 for x in range(10)]

    list_result = []
    for x in range(10):
        if x % 2 == 0:
            list_result.append(x ** 2)
        else:
            list_result.append(x + 2)
    print(list_result)
