S2 = {"A", "B", "C"}
L3 = ["10", "9", "8", "7"]

if __name__ == '__main__':
    print(L3[2::])
    print(L3[:2:])
    print(L3[::2])
    print(L3[2:3:])
    print(L3[:2:1])
    print(L3[1:2:1])
