class Blog:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name


L1 = [1, 2, 3]
L2 = ["A", "B", "C"]
S1 = {1, 2, 3}
S2 = {"A", "B", "C"}
L3 = ["10", "9", "8", "7"]

if __name__ == '__main__':
    print(dict(zip(L1, L2)))
    print(dict(zip(S1, S2)))
    print(dict(zip(S1, L2)))
    print(dict(zip(L2, S1)))
    print(12)
