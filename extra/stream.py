"""
类似 Java 的 Filter 和 Map这些操作
那有没有方便的 Group 操作了,也是有的！
1、for循环
2、多个元素的 for 循环
3、filter
4、map
5、group
"""

items = [25, 68, 12, 99, 34]
items2 = [25, 68, 12, 99, 34]
if __name__ == '__main__':
    # for x in items:
    #     x * x
    # iterms2 = [x ** 2 for x in items if x % 2 == 0]
    # iterms3 = list(map(lambda x: x ** 2, filter(lambda x: x % 2 == 0, items)))
    #
    # print(iterms2)
    # print(iterms3)

    r1 = [(x, y) for x in items for y in items2]
    r3 = {(x, y) for x in items for y in items2}
    r4 = {x: y for x in items for y in items2}
    print(r1)
    print(r3)
    print(r4)
    r2 = zip(items, items2)

    key = "abcde"
    value = range(1, 6)

    dict(zip(key, value))
